/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w10;

/**
 *
 * @author acer
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("Dum");
        Plane plane = new Plane("engine number 1");
        Dog dog = new Dog("Dang");
       
        
        Flyable[] flyable = {bat,plane};
        for(Flyable f:flyable ){
           if(f instanceof Plane ){
               Plane p = (Plane)f;
               p.startEngin();
               p.run();
           }
            f.fly();
        }
        Runable[] runable = {plane,dog};
        for(Runable f: runable){
            f.run();
        }
    }
}
