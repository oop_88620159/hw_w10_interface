/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w10;

/**
 *
 * @author acer
 */
public class Plane extends Vahible implements Flyable,Runable{

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngin() {
       System.out.println("plane: Start");
    }

    @Override
    public void stopEngin() {
        System.out.println("plane: stop");
    }

    @Override
    public void paiseEngin() {
        System.out.println("plane: pasise");
    }

    @Override
    public void applybreak() {
        System.out.println("plane: break");
    }

    @Override
    public void fly() {
        System.out.println("plane: fly");
    }

    @Override
    public void run() {
        System.out.println("plane: run");
    }
    
}
